let adminUser1 = localStorage.getItem("isAdmin");
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')


let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');
let enrolless = document.querySelector('#enrollees')


fetch(`${url}/api/courses/${courseId}`)
.then( res => res.json())
.then( data =>{


	courseName.innerHTML = data.name
	
	if(adminUser1 === "false" || !adminUser1){
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = "Price: $" + data.price
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-success">Enroll</button>`

		let enrollButton = document.querySelector('#enrollButton')
		enrollButton.addEventListener('click', () => {
			fetch(`${url}/api/users/enroll`, {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json',
					'Authorization' : localStorage.getItem('token')
				},
				body: JSON.stringify({ courseId })

			})
			.then( res => res.json())
			.then( data => {
				if (data === true) {
					alert('Thank you for enrollong! See you!')
					window.location.replace('./courses.html')
				} else {
					alert("Something went wrong")
				}
			})
		})

	}else{
		

		

		data.enrollees.forEach(function(enrollee){
			
			fetch(url+ '/api/users/' + enrollee.userId)
			.then(res => res.json())
			.then(data => {

				

			let nameOfEnrollees=[];
			let enrollContainer = document.querySelector('#enrollContainer');
		
			function addLiElement(enrolleeName){
				let div = document.createElement('div')
				div.textContent = enrolleeName
				enrollContainer.appendChild(div);
			}

			nameOfEnrollees.forEach(function(nameEnrollee){
				addLiElement(nameEnrollee);
			})

		
				nameOfEnrollees.push(data.firstName+ " "+data.lastName);
				addLiElement(data.firstName+ " "+data.lastName);


			})
	})
}
})	

