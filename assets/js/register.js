let registerForm = document.querySelector('#registerUser')

registerForm.addEventListener("submit", e => {
	e.preventDefault();

	let firstName = document.querySelector('#firstName').value
	let lastName = document.querySelector('#lastName').value
	let mobileNo = document.querySelector('#mobileNumber').value
	let email = document.querySelector('#userEmail').value
	let password1 = document.querySelector('#password1').value
	let password2 = document.querySelector('#password2').value


	let isFormComplete = password1 === password2 &&
	password1 !== "" &&
	password2 !== "" &&
	firstName !== "" && 
	lastName !== "" &&
	email !== "" &&
	mobileNo.length === 11
	
	// console.log(validationCondition)
	if(isFormComplete) {
		// check for duplicate email
		fetch(url + `/api/users/email-exists`, {
			method : 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email
			})
		})
		.then( res => res.json())
		.then( data => {
			console.log(data)
			if(data === false) {
				fetch(url + `/api/users`,{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify ({
						firstName,
						lastName,
						email,
						password: password1,
						mobileNo
					})

					
				})
				.then( res => res.json())
				.then( data => {
					if (data === true) {
						alert('registered successfully')
						window.location.replace('./login.html');
					}else{
						alert('something went wrong')
					}
				})			
			}else{
				alert('email already in use')
			}
		})
	}else{
		alert("something went wrong")
	}
})