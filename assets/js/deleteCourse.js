let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')

fetch(`${url}/api/courses/${courseId}`, {
	method : "DELETE",
	headers : {
		'Authorization' : localStorage.getItem('token')
	}
})
.then( res => res.json())
.then( data => {
	
	if (data === true) {
		window.location.replace('./courses.html')
	}else{
		alert('something went wrong')
	}
})
