let adminUser = localStorage.getItem("isAdmin");

if(adminUser === 'false'){
fetch(url + '/api/courses')
.then(res => res.json())
.then(data => {
	let coursesContainer = document.querySelector('#coursesContainer');
	let cardFooter = (course) => {
			return `<a href="./course.html?courseId=${course._id}" class="btn btn-success  btn-block">Select Course</a>`
	}
	let courseCard = (params) =>(`
		<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${params.name}</h5>
						<p class="card-text text-left">
							${params.description}
						</p>
						<div class="card-text text-right">
							&#8369;${params.price}
						</div>
					</div>
					<div class="card-footer">
						${cardFooter(params)}
					</div>
				</div>
			</div>
	`)
	let courseData ;
	if (data.length < 1){
		courseData = "No courses available";
	} else {
		courseData = data.map( course => {
			return courseCard(course);
		}).join("")
	}
	coursesContainer.innerHTML = courseData;
})
}else {
	fetch(url + '/api/courses/admin/')
	.then(res => res.json())
	.then(data => {
	let coursesContainer = document.querySelector('#coursesContainer');
	let addCourse = document.querySelector('#addCourse')
	addCourse.innerHTML = `<a href="./addCourse.html" class="btn btn-success text-black "> Add Course </a>`
		let cardFooter = (course) => {
			if(course.isActive === true){
				return `
				<p>Active Course</p>
				<a href="./course.html?courseId=${course._id}" class="btn btn-info  btn-block">View Enrollees</a>
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary  btn-block editButton" >Edit</a>
				<a href="./deleteCourse.html?courseId=${course._id}"class="btn btn-danger  btn-block ">Archive course</a>
				`	
			}else{
				return `
				<p>Archived Course</p>
				<a href="./course.html?courseId=${course._id}" class="btn btn-info  btn-block">View Enrollees</a>
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary  btn-block editButton" >Edit</a>
				<a href="./reenable.html?courseId=${course._id}"class="btn btn-success  btn-block ">Reenable course</a>
				`	
			}	
		}
	let courseCard = (params) =>(`
		<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${params.name}</h5>
						<p class="card-text text-left">
							${params.description}
						</p>
						<div class="card-text text-right">
							&#8369;${params.price}
						</div>
					</div>
					<div class="card-footer">
						${cardFooter(params)}
					</div>
				</div>
			</div>
	`)
	let courseData ;
	if (data.length < 1){
		courseData = "No courses available";
	} else {
		courseData = data.map( course => {
			return courseCard(course);
		}).join("")
	}
	coursesContainer.innerHTML = courseData;
})
}
